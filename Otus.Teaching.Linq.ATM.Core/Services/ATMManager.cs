﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата
        public User GetUser(string login, string pass)
        {
            return Users.Single(user => user.Login == login && user.Password == pass);
        }
        public IEnumerable<Account> GetAccountsByUser(User user)
        {
            return Accounts.Where(acc => acc.UserId == user.Id);
        }
        public Dictionary<Account, IEnumerable<OperationsHistory>> GetAccountsWithHistory(User user)
        {
            return Accounts.Where(acc => acc.UserId == user.Id)
                .ToDictionary(account => account, account =>
                History.Where(history => history.AccountId == account.Id));
        }
        public IEnumerable<User> GetAccountsWithNSumm(decimal cash)
        {
            return Users.Where(user => Accounts.Where(account => account.CashAll > cash).Select(acc => acc.Id).Contains(user.Id));
        }
        public Dictionary<User, Dictionary<Account, IEnumerable<OperationsHistory>>> GetUsersAndYourOperation()
        {
            return Users.ToDictionary(user => user, user => Accounts
            .Where(acc => acc.UserId == user.Id)
            .ToDictionary(account => account, account => History
            .Where(hist => hist.OperationType == OperationType.InputCash && hist.AccountId == account.Id)))
            .Where(record => record.Value.Any(x => x.Value.Any(record => record.OperationType == OperationType.InputCash)))
            .ToDictionary(rec => rec.Key, rec => rec.Value);
        }

    }
}