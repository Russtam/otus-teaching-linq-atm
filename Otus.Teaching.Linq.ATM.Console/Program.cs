﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();
            System.Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю");
            var user = atmManager.GetUser("snow", "111");
            System.Console.WriteLine(user.ToString());
            System.Console.WriteLine();
            System.Console.WriteLine("2. Вывод данных о всех счетах заданного пользователя");
            var accountByUser = atmManager.GetAccountsByUser(user);
            System.Console.WriteLine($"{user} {"\t" + string.Join("\n\t", accountByUser)}");
            System.Console.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту");
            var accountHistory = atmManager.GetAccountsWithHistory(user);
            foreach(var account in accountHistory)
            {
                System.Console.WriteLine(account.Key);
                System.Console.WriteLine("\t"+string.Join("\n\t", account.Value));
                System.Console.WriteLine();
            }
            System.Console.WriteLine();
            System.Console.WriteLine("4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
            var allOperationsToInputCash = atmManager.GetUsersAndYourOperation();
            foreach(var usr in allOperationsToInputCash)
            {

                System.Console.WriteLine(usr.Key);
                System.Console.WriteLine(string.Join("\n", usr.Value.Select(data=>"\t"+string.Join("\n\t", data.Value))));
                System.Console.WriteLine();
            }
            var nSumm = 100.23M;
            System.Console.WriteLine($"5. Вывод данных о всех пользователях у которых на счёте сумма больше {nSumm}");
            var accountsWithNSumm = atmManager.GetAccountsWithNSumm(nSumm);
            System.Console.WriteLine(string.Join("\n", accountsWithNSumm));


            
            System.Console.WriteLine("Завершение работы приложения-банкомата...");
            System.Console.ReadKey();
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}